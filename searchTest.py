import unittest
from search import linear_search
from search import binary_search 

class TestSearch(unittest.TestCase):
#tests for linear search
    def test_search1(self):
        data = [1, 2, 3, 5, 6, 12, 7, 4, 8]
        self.assertEqual(linear_search(data, 6), 4)
        self.assertEqual(linear_search(data, 10),  -1)

    
    def test_searchChar1(self):
       data = ['t', 'a', 'b', 'l', 'e']
       self.assertEqual(linear_search(data, 'a'), 1)
#tests for binary search
    def test_search2(self):
        data = [1, 2, 3, 4,5,6,7,8,10, 9]
        data.sort()
        self.assertEqual(binary_search(data, 6), 5)
        self.assertEqual(binary_search(data, 11),  -1)   

       
    def test_searchChar2(self):
        data = ['t','a', 'b', 'l', 'e']
        data.sort()
        self.assertEqual(binary_search(data, 'a'), 0)
        
    
        

if __name__ == "__main__":
    unittest.main()
