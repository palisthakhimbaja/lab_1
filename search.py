#for linear search
def linear_search(data, target):
    for i in range(len(data)):
        if data[i] == target:
            return i

    return -1
#for binary search
def binary_search(data, target):
    l = 0
    u = len(data)-1
    m = 0
    while l <= u:
        m = (l + u) // 2
        if data[m] < target:
            l = m +1
        elif data[m] > target:
            u = m-1
        else:
            return m 

    return -1
