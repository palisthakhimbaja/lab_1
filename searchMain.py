#this programs finds the best and worst case scenarios and plots the graph 
import random
from search import linear_search
from search import binary_search
import matplotlib.pyplot as plt
from time import time 

worst_linear = []
worst_binary = []
best_linear = []
best_binary = []
item_array = []

#number of items
enum = 400000

#iterating for comparision
for i in range(14):
    #generating data array
    data = random.sample(range(900000000), enum)
    data.sort()

    #Worst case binary
    start = time()
    binary_search(data, data[0])
    end = time()
    worst_time_binary = end - start
    worst_binary.append(worst_time_binary)

    #Worst case linear
    start = time()
    linear_search(data, data[len(data) - 1])
    end = time()
    worst_time_linear = end - start
    worst_linear.append(worst_time_linear)

    #binary search best case
    start = time()
    binary_search(data, data[(len(data) - 1) // 2])
    end = time()
    best_time_binary = end - start
    best_binary.append(best_time_binary)

    #linear search best case
    start = time()
    linear_search(data, data[0])
    end = time()
    best_time_linear = end - start
    best_linear.append(best_time_linear)

    #iteration for next step
    item_array.append(enum)
    enum += 1500000

#plotting the graph
worst_case = plt.figure(1)
plt.plot(item_array, worst_linear, label="linear_worst", color = 'pink')
plt.plot(item_array, worst_binary, label="binary_worst", color = 'blue')
plt.xlabel("List of items")
plt.ylabel("Execution_time(seconds)")
plt.title("Worst Case: Binary v/s Linear")

best_case = plt.figure(2)
plt.plot(item_array, best_linear, label="linear_best", color = 'blue')
plt.plot(item_array, best_binary, label="binary_best", color = 'yellow')
plt.xlabel("list of items")
plt.ylabel("execution-time(seconds)")
plt.title("Best Case: Linear v/s Binary")

plt.legend()
plt.show()

print("Linear times(worst-case): ",worst_linear)
print("Binary times(worst-case): ",worst_binary)
print("Linear times(best-case): ",best_linear)
print("Binary times(best-case): ",best_binary)
